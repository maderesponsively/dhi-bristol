<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wufee_ft extends EE_Fieldtype 
{
	private $model;
	
	public $info = array(
		'name'		=> 'Wufee',
		'version'	=> '1.0'
		);
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

        $this->EE->load->model('wufee_model');
        $this->model = $this->EE->wufee_model;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Display Field on Publish
	 *
	 * @access	public
	 * @param	existing data
	 * @return	field html
	 *
	 */
	function display_field($data)
	{		
		$options = $this->model->get_form_options();
		$options = array('' => 'Select a form') + $options;
		
		return form_dropdown($this->field_name, $options, $data, 'id="' . $this->field_id . '"');
	}
	
	// --------------------------------------------------------------------
		
	/**
	 * Replace tag
	 *
	 * @access	public
	 * @param	field contents
	 * @return	replacement text
	 *
	 */
	function replace_tag($data, $params = array(), $tagdata = FALSE)
	{
		$xform = $this->model->get_form($data);
		
		$page_data = array(
			'subdomain' 	=> $this->model->get_wufoo_subdomain(),
			'form' 			=> unserialize($xform->data),
			);
		
		$view = 'embed_js';
		/*
		if(array_key_exists('method', $params) && $params['method'] == 'iframe')
		{
			$page_data['height'] = array_key_exists('height', $params) ? $params['height'] : '100%';
			$view = 'embed_iframe';
		}
		*/
		
		return $this->EE->load->view($view, $page_data, TRUE);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Install Fieldtype
	 *
	 * @access	public
	 * @return	default global settings
	 *
	 */
	function install()
	{
		return array();
	}
}

/* End of file ft.wufee.php */
/* Location: ./system/expressionengine/third_party/wufee/ft.wufee.php */