<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(	
	'wufee_module_name' => 
	'Wufee',

	'wufee_module_description' => 
	'Your Wufoo forms and data now accessable through EE.',

	'module_home' 				=> 'Wufee',
	
	'dashboard'					=> 'Dashboard',
	'public'					=> 'Public',
	'private'					=> 'Private',
	'loading'					=> 'Loading',
	'showing'					=> 'Showing',
	'total_entries'				=> 'total entries',
	'view'						=> 'View',
	'entries'					=> 'Entries',
	'yes'						=> 'Yes',
	'no'						=> 'No',
	
	'title_settings'			=> 'Wufee Settings',
	'title_view'				=> 'View Form',
	'title_entries'				=> 'Form Entries',
	'title_columns'				=> 'Display Columns',
	'title_entry'				=> 'Form Entry',
	'title_fields'				=> 'Form Fields',
	
	'btn_refresh' 				=> 'Refresh Data',
	'btn_wufoo_admin' 			=> 'Wufoo Admin',
	'btn_settings'				=> 'Settings',
	'btn_back_forms'			=> 'Back to Forms',
	'btn_back_entries'			=> 'Back to Entries',
	'btn_update_settings'		=> 'Update Settings',
	'btn_edit_columns'			=> 'Edit Columns',
	'btn_save_settings'			=> 'Save Settings',
	'btn_view_entries'			=> 'View Form Entries',
	'btn_view_fields'			=> 'View Form Fields',
	
	'h3_my_forms'				=> 'My Forms',
	'h3_my_reports'				=> 'My Reports',
	
	'cap_settings'				=> 'General Settings',
	'cap_details'				=> 'Entry Details',
	
	'th_name'					=> 'Name',
	'th_access'					=> 'Access',
	'th_created'				=> 'Created',
	'th_updated'				=> 'Updated',
	'th_entries'				=> 'Entries',
	'th_options'				=> 'Options',
	'th_field'					=> 'Field',
	'th_value'					=> 'Value',
	'th_title'					=> 'Title',
	'th_id'						=> 'ID',
	'th_type'					=> 'Type',
	'th_required'				=> 'Is required',
	
	'lbl_10_min'				=> 'Every 10 min.',
	'lbl_15_min'				=> 'Every 15 min.',
	'lbl_20_min'				=> 'Every 20 min.',
	'lbl_30_min'				=> 'Every 30 min.',
	'lbl_45_min'				=> 'Every 45 min.',
	'lbl_60_min'				=> 'Every 60 min.',
	'lbl_subdomain'				=> 'Wufoo account subdomain',
	'lbl_api_key'				=> 'Wufoo account API key',
	'lbl_refresh'				=> 'API data refresh rate',
	'lbl_date_updated'			=> 'Date Updated',
	
	'flash_settings_good'		=> 'Your settings have been updated.',
	'flash_columns_good'		=> 'Display columns have been updated.',
	
	'txt_loading_1'				=> 'Your Wufoo data is being imported. This may take a few minutes depending on the amount of data to import.',
	'txt_loading_2'				=> '<strong>Tip:</strong> Try adjusting the API refresh rate in the settings so the sync happens less frequently.',
	'txt_new_window'			=> 'Open in new window',
	
	'wgt_forms_name' => 'Wufoo Forms',
	'wgt_forms_description' => 'Wufee widget displaying your forms and # of entries.',
	
	'wgt_reports_name' => 'Wufoo Reports',
	'wgt_reports_description' => 'Wufee widget displaying links to your Wufoo reports.',
	
// Start inserting custom language keys/values here
	
);

/* End of file lang.wufee.php */
/* Location: /system/expressionengine/third_party/wufee/language/english/lang.wufee.php */
