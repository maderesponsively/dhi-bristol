<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Wufee Module Install/Update File
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		Chris Monnat
 * @link		http://chrismonnat.com
 */

class Wufee_upd {
	
	public $version = '1.0';
	
	private $_EE;
	private $_model;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->_EE =& get_instance();
		
        $this->_EE->load->add_package_path(PATH_THIRD .'wufee/');

        $this->_EE->load->model('wufee_model');
        $this->_model = $this->_EE->wufee_model;
        
        $this->version = $this->_model->get_package_version();
	}
	
	// ----------------------------------------------------------------
	
	/**
	 * Installation Method
	 *
	 * @return 	boolean 	TRUE
	 */
	public function install()
	{
		return $this->_model->install_module();
	}

	// ----------------------------------------------------------------
	
	/**
	 * Uninstall
	 *
	 * @return 	boolean 	TRUE
	 */	
	public function uninstall()
	{
		return $this->_model->uninstall_module();
	}
	
	// ----------------------------------------------------------------
	
	/**
	 * Module Updater
	 *
	 * @return 	boolean 	TRUE
	 */	
	public function update($current = '')
	{
		return $this->_model->update_package($current);	
	}
	
}
/* End of file upd.wufee.php */
/* Location: /system/expressionengine/third_party/wufee/upd.wufee.php */