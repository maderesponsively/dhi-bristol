<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Wufee Forms Module Accessory File
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Accessory
 * @author		Chris Monnat
 * @link		http://chrismonnat.com
 */

class Wufee_acc 
{
	public $EE;
	public $model;
    public $name       		= 'Wufee Forms';
    public $id 				= 'wufee_forms';
    public $version 		= '1.0';
    public $description 	= 'Accessory showing forms summary listing.';
    public $sections 		= array();

    public function set_sections()
    {
        $this->EE =& get_instance();
        
        $this->EE->load->library('table');
        $this->EE->load->model('wufee_model');
        $this->EE->load->helper('wufee');
                
        $this->model = $this->EE->wufee_model;
        
		$page_data = array(
			'base_url'		=> $this->model->get_base_url(),
			'theme_url'		=> $this->model->get_package_theme_url(),
			'account_url'	=> $this->model->get_wufoo_account_url(),
			'forms' 		=> $this->model->get_forms()
			);
	
	    $this->sections['Wufoo Forms'] = $this->EE->load->view('acc_forms', $page_data, TRUE);
    }    
}
/* End of file acc.wufee_forms.php */
/* Location: /system/expressionengine/third_party/wufee/acc.wufee_forms.php */