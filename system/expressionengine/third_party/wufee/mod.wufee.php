<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Wufee Module Front End File
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		Chris Monnat
 * @link		http://chrismonnat.com
 */

class Wufee {
	
	public $return_data;
	
	private $_EE;
	private $_model;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->_EE =& get_instance();

        $this->_EE->load->model('wufee_model');
        $this->_model = $this->_EE->wufee_model;
	}
	
	// ----------------------------------------------------------------

	/**
	 * Generate Wufoo Form Embed Code
	 * {exp:wufee:form form_url="required" method="optional" height="optional"}
	 *
	 * @return 	string
	 */
	public function form()
	{
		$xform = $this->_model->get_form_by_url($this->_EE->TMPL->fetch_param('form_url'));

		$page_data = array(
			'subdomain' 	=> $this->_model->get_wufoo_subdomain(),
			'form' 			=> unserialize($xform->data),
			);
		
		$view = 'embed_js';
		if($this->_EE->TMPL->fetch_param('method') == 'iframe')
		{
			$height = $this->_EE->TMPL->fetch_param('height');			
			$page_data['height'] = $height != '' ? $height : '100%';
			$view = 'embed_iframe';
		}
		
		return $this->_EE->load->view($view, $page_data, TRUE);
	}
	
	// ----------------------------------------------------------------

	/**
	 * Display Wufoo Form Entries
	 * {exp:wufee:entries form_url="required" limit="optional"}{/exp:wufee:entries}
	 *
	 * @return 	string
	 */
	public function entries()
	{
		$xform = $this->_model->get_form_by_url($this->_EE->TMPL->fetch_param('form_url'));
		$entries = $this->_model->get_entries($xform->hash, $this->_EE->TMPL->fetch_param('limit'));
		
		$tagdata = array();
		$i = 0;
		foreach($entries as $xentry)
		{
			$entry = unserialize($xentry->data);
			foreach($entry as $var => $value)
			{
				$tagdata[$i][$var] = $value;
			}
			
			++$i;
		}
		
		return $this->_EE->TMPL->parse_variables($this->_EE->TMPL->tagdata, $tagdata);
	}
}
/* End of file mod.wufee.php */
/* Location: /system/expressionengine/third_party/wufee/mod.wufee.php */