<?php if(count($reports) > 0): ?>
	<?php foreach($reports as $xreport): ?>
		<?php $report = unserialize($xreport->data); ?>
			<p><?php echo anchor($wufoo_url . 'reports/' . $report->Url, $report->Name, 'target="_blank"'); ?></p>
	<?php endforeach; ?>
<?php else: ?>
	<p>You don't currently have access to any reports.</p>
<?php endif; ?>