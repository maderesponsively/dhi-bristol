<?php echo form_open($base_qs . AMP . 'method=update_columns' . AMP . 'form_id=' . $form_id); ?>

	<?php foreach($fields->Fields as $id => $field): ?>
		<p><label><?php echo form_checkbox('columns[]', $field->ID, in_array($field->ID, $columns)); ?> <?php echo $field->Title; ?></label></p>
	<?php endforeach; ?>
	
	<input type="hidden" name="form_id" value="<?php echo $form_id; ?>" />
	
	<p><input type="submit" value="<?php echo lang('btn_save_settings'); ?>" /></p>

<?php echo form_close(); ?>