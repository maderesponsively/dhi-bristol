<div id="wufoo-<?php echo $form->Hash; ?>">
Fill out my <a href="http://<?php echo $subdomain; ?>/forms/<?php echo $form->Hash; ?>">online form</a>.
</div>
<script type="text/javascript">var w7x3k1;(function(d, t) {
var s = d.createElement(t), options = {
'userName':'<?php echo $subdomain; ?>', 
'formHash':'<?php echo $form->Hash; ?>', 
'autoResize':true,
'height':'724',
'async':true,
'header':'hide'};
s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'wufoo.com/scripts/embed/form.js';
s.onload = s.onreadystatechange = function() {
var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
try { w7x3k1 = new WufooForm();w7x3k1.initialize(options);w7x3k1.display(); } catch (e) {}};
var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
})(document, 'script');</script>