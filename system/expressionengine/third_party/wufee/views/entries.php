<p><?php echo $showing; ?></p>

<?php

$this->table->set_template($cp_table_template);

$this->table->set_heading($heading);

foreach($entries as $id => $entry)
{
	$entry = unserialize($entry->data);
	
	$row = array();
	foreach($display as $id)
	{
		if($id != 'DateCreated' AND $id != 'DateUpdated')
		{
			$row[] = $entry->$id != '' ? auto_link($entry->$id, 'url', TRUE) : '--';
		}
		else
		{
			$row[] = datetime_to_human($entry->$id);
		}
	}
	$row[] = anchor($base_url . AMP . 'method=entry' . AMP . 'form_id=' . $form->Hash . AMP . 'entry_id=' . $entry->EntryId . AMP . 'p=' . $p, lang('view'));
	
	$this->table->add_row($row);
}

echo $this->table->generate();

echo $page_nav;