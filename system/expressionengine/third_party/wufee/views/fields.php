<p>Below are the fields for the <strong><?php echo $form->Name; ?></strong> form. The Url Title and ID column below are used in conjunction with the 
{exp:wufee:form} and {exp:wufee_entries} tag pair when using the module in your templates.</p>

<h3>Subdomain: <?php echo $form->Url; ?></h3>

<?php

$this->table->set_template($cp_table_template);

$this->table->set_heading(
   lang('th_title'),
   lang('th_id'),
   lang('th_type'),
   lang('th_required')
   );

foreach($fields->Fields as $field)
{
	if(isset($field->IsRequired) AND $field->IsRequired == 1)
	{
		$required = lang('yes');
	}
	elseif(isset($field->IsRequired) AND $field->IsRequired == 0)
	{
		$required = lang('no');
	}
	else
	{
		$required = '--';
	}
	
	$this->table->add_row(
		$field->Title,
		$field->ID,
		ucfirst($field->Type),
		$required
		);
}

echo $this->table->generate();
$this->table->clear();