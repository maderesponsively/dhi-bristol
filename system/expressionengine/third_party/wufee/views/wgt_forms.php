<table>
	<thead><tr><th width="65%">Name</th><th># Entries</th></tr></thead>
	<tbody>
	<?php if(count($forms) > 0): ?>
		<?php foreach($forms as $xform): ?>
			<?php $form = unserialize($xform->data); ?>
			<tr class="<?php echo alternator('odd','even'); ?>">
				<td><?php echo anchor($base_url . AMP . 'method=entries' . AMP . 'form_id=' . $form->Hash, $form->Name); ?></td>
				<td><?php echo $form->TotalEntries; ?></td>
			</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr><td colspan="2"><center>You don't currently have access to any forms.</center></td></tr>
	<?php endif; ?>
	</tbody>
</table>