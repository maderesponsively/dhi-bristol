<?php 

echo form_open($base_qs . AMP . 'method=update_settings');

$this->table->set_template($cp_pad_table_template);
$this->table->template['thead_open'] = '<thead class="visualEscapism">';

$this->table->set_caption(lang('cap_settings'));

$this->table->add_row(
   lang('lbl_subdomain') . ':',
   form_input('subdomain', set_value('subdomain', @$settings->subdomain))
   );

$this->table->add_row(
   lang('lbl_api_key') . ': (' . anchor('https://mrtopher.wufoo.com/docs/api/v3/#key', '?', 'target="_blank"') . ')',
   form_input('api_key', set_value('api_key', @$settings->api_key))
   );

$this->table->add_row(
   lang('lbl_refresh') . ':',
   form_dropdown('refresh', $opts_refresh_rate, set_value('refresh', @$settings->refresh))
   );

echo $this->table->generate();

?>

<div class="tableFooter">
	<?php echo form_submit(array('name' => 'submit', 'value' => lang('btn_update_settings'), 'class' => 'submit')); ?>
</div>