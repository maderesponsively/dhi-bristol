<?php

$this->table->set_template($cp_table_template);

$this->table->set_heading(
   lang('th_field'),
   lang('th_value')
   );

foreach($field_data as $row)
{
	$this->table->add_row($row);
}

echo $this->table->generate();
$this->table->clear();

?>

<p>&nbsp;</p>

<?php

$this->table->template['thead_open'] = '<thead class="visualEscapism">';

$this->table->set_caption(lang('cap_details'));

foreach($entry_data as $row)
{
	$this->table->add_row($row);
}

echo $this->table->generate();

?>