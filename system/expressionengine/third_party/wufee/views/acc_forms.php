<?php

$this->table->set_heading(
   'Name',
   'Entries',
   'Access',
   'Created',
   'Updated'
   );

foreach($forms as $form)
{
	$form = unserialize($form->data);
	
	$entries = anchor($base_url . AMP . 'method=entries' . AMP . 'form_id=' . $form->Hash, $form->TotalEntries);
	if($form->EntryCountToday > 0)
	{
		$entries .= ' - ' . $form->EntryCountToday . ' Today';
	}
	
	$this->table->add_row(
		anchor($base_url . AMP . 'method=view' . AMP . 'form_id=' . $form->Hash, $form->Name) . ' &nbsp;&nbsp;&nbsp;' . anchor($account_url . 'forms/' . $form->Url, '<img src="'.$theme_url.'images/icon-form-window.png" alt="#" />', 'target="_blank" title="' . lang('txt_new_window') . '"'),
		$entries,
		$form->IsPublic ? 'Public' : 'Private',
		datetime_to_human($form->DateCreated),
		datetime_to_human($form->DateUpdated)
		);
}

echo $this->table->generate();
$this->table->clear();