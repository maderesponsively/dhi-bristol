<h3><?php echo lang('h3_my_forms'); ?> (<?php echo count($forms); ?>)</h3>

<?php

$this->table->set_template($cp_table_template);

$this->table->set_heading(
   lang('th_name'),
   lang('th_entries'),
   lang('th_access'),
   lang('th_created'),
   lang('th_updated')
   );

foreach($forms as $form)
{
	$form = unserialize($form->data);
	
	$entries = anchor($base_url . AMP . 'method=entries' . AMP . 'form_id=' . $form->Hash, $form->TotalEntries);
	if($form->EntryCountToday > 0)
	{
		$entries .= ' - ' . $form->EntryCountToday . ' Today';
	}
	
	$this->table->add_row(
		anchor($base_url . AMP . 'method=view' . AMP . 'form_id=' . $form->Hash, $form->Name) . ' &nbsp;&nbsp;&nbsp;' . anchor($account_url . 'forms/' . $form->Url, '<img src="'.$theme_url.'images/icon-form-window.png" alt="#" />', 'target="_blank" title="' . lang('txt_new_window') . '"'),
		$entries,
		$form->IsPublic ? lang('public') : lang('private'),
		datetime_to_human($form->DateCreated),
		datetime_to_human($form->DateUpdated)
		);
}

echo $this->table->generate();
$this->table->clear();

?>

<br />
<h3><?php echo lang('h3_my_reports'); ?> (<?php echo count($reports); ?>)</h3>

<?php

$this->table->set_template($cp_table_template);

$this->table->set_heading(
   lang('th_name'),
   lang('th_access'),
   lang('th_name'),
   lang('th_updated')
   );

foreach($reports as $xreport)
{
	$report = unserialize($xreport->data);
	
	$this->table->add_row(
		anchor($account_url . 'reports/' . $report->Url, $report->Name, 'target="_blank"'),
		$report->IsPublic ? lang('public') : lang('private'),
		datetime_to_human($report->DateCreated),
		datetime_to_human($report->DateUpdated)
		);
}

echo $this->table->generate();