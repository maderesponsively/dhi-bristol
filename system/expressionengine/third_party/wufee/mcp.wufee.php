<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Wufee Module Control Panel File
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		Chris Monnat
 * @link		http://chrismonnat.com
 */

class Wufee_mcp {
	
	public $return_data;
	
	private $_EE;
	private $_model;
	private $_base_qs;
	private $_base_url;
	private $_theme_url;
	private $_member_id;
	private $_settings;
		
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->_EE =& get_instance();
		
        $this->_EE->load->model('wufee_model');
        $this->_model = $this->_EE->wufee_model;
		
        $this->_base_qs     = $this->_model->get_base_qs();;
        $this->_base_url    = $this->_model->get_base_url();
        $this->_theme_url   = $this->_model->get_package_theme_url();
        $this->_css_url   	= $this->_theme_url .'css/main.css';
        $this->_js_url   	= $this->_theme_url .'js/wufee.js';
        $this->_member_id	= $this->_EE->session->userdata('member_id');
		$this->_settings    = $this->_model->get_member_settings($this->_member_id);
	}
	
	// ----------------------------------------------------------------

	/**
	 * Index Function
	 *
	 * @return 	void
	 */
	public function index()
	{		
		// check to make sure user has existing module settings
		// display settings form if they dont
		if(!$this->_settings)
		{
			return $this->settings();
		}
		else
		{
			$this->_EE->load->helper('wufee');
						
			$this->_EE->cp->set_variable('cp_page_title', lang('dashboard'));
			
			$this->_EE->cp->set_breadcrumb($this->_base_url, lang('module_home'));	
			
			$this->_EE->cp->set_right_nav(array(
				'btn_refresh' 		=> $this->_base_url . AMP . 'refresh=yes',
				'btn_wufoo_admin' 	=> $this->_model->get_wufoo_account_url() . 'admin',
				'btn_settings' 		=> $this->_base_url . AMP . 'method=settings',
				));
				
			// left align refresh button
			$this->_EE->javascript->output("
				$('a.submit[title=\'" . lang('btn_refresh') . "\']').parent('.button').css('float', 'left');
				$('a:contains(\"Wufoo Admin\")').attr('target', '_blank');
				");
				
			$this->_EE->javascript->compile();

			if(is_null($this->_settings->last_sync) OR (date('Y-m-d H:i:s', (strtotime($this->_settings->last_sync)+$this->_settings->refresh*60)) < date('Y-m-d H:i:s')) OR $this->_EE->input->get('refresh') == 'yes')
			{
				$this->_EE->cp->add_to_foot('<script type="text/javascript" src="' . $this->_theme_url . 'js/urlvars.js"></script>');
				$this->_EE->cp->add_to_foot('<script type="text/javascript" src="' . $this->_theme_url . 'js/load-forms.js"></script>');
			
				$page_data = array(
					'theme_url'	=> $this->_theme_url
					);
		
				return $this->_EE->load->view('loading', $page_data, TRUE);
			}
			else
			{
				return $this->show_forms();
			}		
		}
	}
	
	/**
	 * Show Forms Function
	 * Gets Wufoo forms from DB and displays them.
	 *
	 * @return 	void
	 */
	public function show_forms()
	{
		$this->_EE->load->library('table');
		$this->_EE->load->helper('wufee');
		
		if($this->_EE->input->get('refresh') == 'yes')
		{
			$this->_model->sync_forms(TRUE);
		}
		else
		{
			$this->_model->sync_forms();
		}
		
		$page_data = array(
			'base_url'		=> $this->_base_url,
			'theme_url'		=> $this->_model->get_package_theme_url(),
			'account_url'	=> $this->_model->get_wufoo_account_url(),
			'reports'		=> $this->_model->get_reports(),
			'forms' 		=> $this->_model->get_forms()
			);
		
		if($this->_EE->input->get('ajax') == 'yes')
		{
			echo $this->_EE->load->view('index', $page_data, TRUE);
			exit();
		}
		else
		{
			return $this->_EE->load->view('index', $page_data, TRUE);
		}
	}
	
	/**
	 * Display module settings form.
	 *
	 * @return 	void
	 */
	public function settings()
	{
		$this->_EE->load->library('table');
		
		$opts_refresh_rate = array(
			10 => lang('lbl_10_min'),
			15 => lang('lbl_15_min'),
			20 => lang('lbl_20_min'),
			30 => lang('lbl_30_min'),
			45 => lang('lbl_45_min'),
			60 => lang('lbl_60_min')
			);
			
		$this->_EE->cp->set_variable('cp_page_title', lang('title_settings'));
		
		$this->_EE->cp->set_breadcrumb($this->_base_url, lang('module_home'));
		
		$this->_EE->cp->set_right_nav(array(
			'btn_back_forms' => $this->_base_url
			));
			
		$page_data = array(
			'base_qs'			=> $this->_base_qs,
			'opts_refresh_rate' => $opts_refresh_rate,
			'settings'			=> $this->_settings
			);
			
		return $this->_EE->load->view('settings', $page_data, TRUE);
	}
	
	/**
	 * Attempt to update users settings in DB.
	 *
	 * @return 	void
	 */
	public function update_settings()
	{
		$this->_EE->load->library('form_validation');
		$this->_EE->load->helper('flash');
		
		$this->_EE->form_validation->set_rules(array(
			array('field' => 'subdomain', 'label' => 'wufoo account subdomain', 'rules' => 'trim|required|htmlentities'),
			array('field' => 'api_key', 'label' => 'wufoo account API key', 'rules' => 'trim|required'),
			array('field' => 'refresh', 'label' => 'API data refresh rate', 'rules' => 'trim|required|numeric|htmlentities'),
			));
			
		if($this->_EE->form_validation->run())
		{
			$params = array(
				'subdomain' => $this->_EE->input->post('subdomain'),
				'api_key' 	=> $this->_EE->input->post('api_key'),
				'refresh' 	=> $this->_EE->input->post('refresh'),
				);
				
			$this->_model->update_member_settings($params);
			
			set_flash('wufee', 'success', lang('flash_settings_good'));
			$this->_EE->functions->redirect($this->_model->get_module_url());
		}
		else
		{
			$this->settings();
		}
	}
	
	/**
	 * Display selected form.
	 *
	 * @param	form_id
	 * @return 	void
	 */
	public function view()
	{		
		$form = $this->_model->get_form($this->_EE->input->get('form_id'));
	
		$this->_EE->cp->set_variable('cp_page_title', lang('title_view'));
		
		$this->_EE->cp->set_breadcrumb($this->_base_url, lang('module_home'));
		
		$this->_EE->cp->set_right_nav(array(
			'btn_view_entries' => $this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $form->hash, 
			'btn_back_forms' => $this->_base_url,
			));
		
		$page_data = array(
			'subdomain' 	=> $this->_model->get_wufoo_subdomain(),
			'form' 			=> unserialize($form->data),
			);
			
		return $this->_EE->load->view('embed_js', $page_data, TRUE);
	}
	
	/**
	 * Entry listing for selected form.
	 *
	 * @param	form_id
	 * @return 	void
	 */
	public function entries()
	{			
		$xform = $this->_model->get_form($this->_EE->input->get('form_id'));
		$form = unserialize($xform->data);
		
		$this->_EE->cp->set_variable('cp_page_title', $form->Name . ' ' . lang('title_entries'));
		
		$this->_EE->cp->set_breadcrumb($this->_base_url, lang('module_home'));
	
		$this->_EE->cp->set_right_nav(array(
			'btn_refresh' => $this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $xform->hash . AMP . 'refresh=yes',
			'btn_edit_columns' => $this->_base_url . AMP . 'method=columns' . AMP . 'form_id=' . $form->Hash,
			'btn_view_fields' => $this->_base_url . AMP . 'method=fields' . AMP . 'form_id=' . $form->Hash,
			'btn_back_forms' => $this->_base_url,
			));
			
		// left align refresh button
		$this->_EE->javascript->output("
			$('a.submit[title=\'Refresh Data\']').parent('.button').css('float', 'left');
			");
						
		$this->_compile_flash_msgs('wufee_msg');
				
		if((is_null($xform->last_sync) OR date('Y-m-d H:i:s', (strtotime($xform->last_sync)+$this->_settings->refresh*60)) < date('Y-m-d H:i:s')) OR $this->_EE->input->get('refresh') == 'yes')
		{		
			$this->_EE->cp->add_to_head('<script type="text/javascript" src="' . $this->_theme_url . 'js/urlvars.js"></script>');
			$this->_EE->cp->add_to_head('<script type="text/javascript" src="' . $this->_theme_url . 'js/load-entries.js"></script>');

			$page_data = array(
				'theme_url'	=> $this->_theme_url
				);
	
			return $this->_EE->load->view('loading', $page_data, TRUE);
		}
		else
		{
			return $this->show_entries($xform);
		}
	}
	
	/**
	 * Show Entries Function
	 * Gets Wufoo entries from DB and displays them.
	 *
	 * @return 	void
	 */
	public function show_entries($xform = NULL)
	{
		$this->_EE->load->library('table');
		$this->_EE->load->library('pagination');
		$this->_EE->load->helper('wufee');
		
		$xform = is_null($xform) ? $this->_model->get_form($this->_EE->input->get('form_id')) : $xform;
		
		if($this->_EE->input->get('refresh') == 'yes')
		{
			$this->_model->sync_entries($xform->hash, TRUE);
		}
		else
		{
			$this->_model->sync_entries($xform->hash);
		}

		$form 		= unserialize($xform->data);
		$fields 	= unserialize($xform->fields);
		$display 	= explode('|', $xform->display_fields);
		
		$heading = array();
		foreach($display as $val)
		{
			$heading[] = $fields->Fields[$val]->Title;
		}
		$heading[] = lang('th_options');
		
		// pagination
        $theme_url = $this->_EE->config->item('theme_folder_url');
        $theme_url .= substr($theme_url, -1) == '/' ? 'cp_themes/' : '/cp_themes/'; 
		
		$page = $this->_EE->input->get('p');
		$per_page = 25;
		$page_config = array(
			'base_url' 				=> $this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $xform->hash,
			'total_rows' 			=> $form->TotalEntries,
			'per_page' 				=> $per_page,
			'page_query_string' 	=> TRUE,
			'query_string_segment' 	=> 'p',
			'full_tag_open'			=> '<p id="paginationLinks">',
			'prev_link'				=> '<img width="13" height="13" alt=">" src="' . $theme_url . 'default/images/pagination_prev_button.gif">',
			'next_link'				=> '<img width="13" height="13" alt=">" src="' . $theme_url . 'default/images/pagination_next_button.gif">',
			);
		
		$this->_EE->pagination->initialize($page_config);
		
		$entries = $this->_model->get_entries($form->Hash, $per_page, $page);
		
		$page_data = array(
			'base_url'	=> $this->_base_url,
			'showing'	=> lang('showing') . ' ' . ($page + 1) . ' - ' . ($page + count($entries)) . ' of ' . $form->TotalEntries . ' ' . lang('total_entries') . '.',
			'form'		=> $form,
			'display'	=> explode('|', $xform->display_fields),
			'heading'	=> $heading,
			'entries' 	=> $entries,
			'page_nav'	=> $this->_EE->pagination->create_links(),
			'p'			=> $page
			);
			
		if($this->_EE->input->get('ajax') == 'yes')
		{
			echo $this->_EE->load->view('entries', $page_data, TRUE);
			exit();
		}
		else
		{
			return $this->_EE->load->view('entries', $page_data, TRUE);
		}
	}
	
	/**
	 * Display columns currently displayed for form and allow user to change them.
	 *
	 * @param	form_id
	 * @return 	void
	 */
	public function columns()
	{
		$xform = $this->_model->get_form($this->_EE->input->get('form_id'));
		$form = unserialize($xform->data);

		$this->_EE->cp->set_variable('cp_page_title', lang('title_columns'));
		
		$this->_EE->cp->set_breadcrumb($this->_base_url, lang('module_home'));
		$this->_EE->cp->set_breadcrumb($this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $xform->hash, $form->Name);
		
		$this->_EE->cp->set_right_nav(array(
			'Go back' => $this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $form->Hash,
			));
		
		$page_data = array(
			'base_qs'	=> $this->_base_qs,
			'form_id'	=> $xform->hash,
			'fields'	=> unserialize($xform->fields),
			'columns' 	=> explode('|', $xform->display_fields)
			);
			
		return $this->_EE->load->view('columns', $page_data, TRUE);
	}
	
	/**
	 * Display form field data.
	 *
	 * @param	form_id
	 * @return 	void
	 */
	public function fields()
	{
		$this->_EE->load->library('table');
	
		$xform = $this->_model->get_form($this->_EE->input->get('form_id'));
		$form = unserialize($xform->data);
		
		$this->_EE->cp->set_variable('cp_page_title', lang('title_fields'));
		
		$this->_EE->cp->set_breadcrumb($this->_base_url, lang('module_home'));
		$this->_EE->cp->set_breadcrumb($this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $xform->hash, $form->Name);
		
		$this->_EE->cp->set_right_nav(array(
			'Go back' => $this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $form->Hash,
			));
		
		$page_data = array(
			'form'		=> $form,
			'fields'	=> unserialize($xform->fields)
			);
			
		return $this->_EE->load->view('fields', $page_data, TRUE);
		
	}
	
	/**
	 * Update display columns for selected form.
	 *
	 * @return 	void
	 */
	public function update_columns()
	{
		$this->_EE->load->library('form_validation');
		
		$this->_EE->form_validation->set_rules(array(
			array('field' => 'form_id', 'label' => 'form', 'rules' => 'trim|required'),
			array('field' => 'columns[]', 'label' => 'columns', 'rules' => 'trim|required'),
			));
			
		if($this->_EE->form_validation->run())
		{
			$columns = $this->_EE->input->post('columns');
			$display = '';
			$i = 1;
			foreach($columns as $column)
			{
				$display .= $column;
				
				if($i < count($columns))
				{
					$display .= '|';
					++$i;
				}
			}
		
			$params = array(
				'display_fields' => $display
				);
				
			$this->_model->update_form($this->_EE->input->post('form_id'), $params);
			
			$this->_EE->session->set_flashdata('wufee_msg', lang('flash_columns_good'));
			
			$this->_EE->functions->redirect($this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $this->_EE->input->post('form_id'));
		}
		else
		{
			$this->columns();
		}
	}
	
	/**
	 * Show detail for selected entry.
	 *
	 * @param	form_id
	 * @param	entry_id
	 * @return 	void
	 */
	public function entry()
	{
		$this->_EE->load->library('table');
		$this->_EE->load->helper('wufee');
	
		$xform 	= $this->_model->get_form($this->_EE->input->get('form_id'));
		$form	= unserialize($xform->data);	
		$fields = $this->_get_field_options(unserialize($xform->fields));
		$fields['DateUpdated'] = lang('lbl_date_updated'); // added to get rid of error when not provided by API

		$entry = $this->_model->get_entry($form->Hash, $this->_EE->input->get('entry_id'));
		$field_data = array();
		$entry_data = array();
		foreach($entry as $field => $value)
		{
			if(preg_match('/Field[0-9]+/', $field))
			{
				$field_data[] = array(
					array('data' => $fields[$field], 'width' => '25%'),
					$value ? auto_link(nl2br($value), 'both', TRUE) : '--'
					);			
			}
			else
			{
				if($field == 'DateCreated' OR $field == 'DateUpdated')
				{
					$value = datetime_to_human($value);
				}
			
				$entry_data[] = array(
					array('data' => $fields[$field], 'width' => '25%'),
					$value ? auto_link($value, 'both', TRUE) : '--'
					);			
			}
		}
		
		$this->_EE->cp->set_variable('cp_page_title', $form->Name . ' ' . lang('title_entry') . ' #' . $entry->EntryId);
		
		$this->_EE->cp->set_breadcrumb($this->_base_url, lang('module_home')); 
		$this->_EE->cp->set_breadcrumb($this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $form->Hash, lang('entries')); 
		
		$this->_EE->cp->set_right_nav(array(
			'btn_back_entries' => $this->_base_url . AMP . 'method=entries' . AMP . 'form_id=' . $form->Hash . AMP . 'p=' . $this->_EE->input->get('p')
			));
		
		$page_data = array(
			'base_url'	=> $this->_base_url,
			'form'		=> $form,
			'fields'	=> $fields,
			'field_data'=> $field_data,
			'entry_data'=> $entry_data
			);
			
		return $this->_EE->load->view('entry', $page_data, TRUE);
	}
	
	/**
	 * Translates Wufoo object field array into key => value format.
	 *
	 * @param	fields
	 * @return 	array
	 */
	private function _get_field_options($fields)
	{
		$options = array();
		foreach($fields->Fields as $id => $params)
		{
			$options[$id] = $params->Title;
			if(isset($params->SubFields)) 
			{
				foreach($params->SubFields as $subfield) 
				{
					$options[$subfield->ID] = $subfield->Label;
				}
			}
		}
		
		return $options;
	}
	
	/**
	 * Compile and display any available module flash messages.
	 *
	 * @return 	void
	 */
	private function _compile_flash_msgs($flashvar)
	{
		$msg = $this->_EE->session->flashdata($flashvar);
		if($msg != '')
		{
			$this->_EE->javascript->output("
				$.ee_notice('".$msg."', {type: 'success'});
			");
		}
		
		$this->_EE->javascript->compile();
	}
	
}
/* End of file mcp.wufee.php */
/* Location: /system/expressionengine/third_party/wufee/mcp.wufee.php */