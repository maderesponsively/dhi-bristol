<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Wufee Model
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		Chris Monnat
 * @link		http://chrismonnat.com
 */

require_once(PATH_THIRD . 'wufee/libraries/WufooApiWrapper.php');

class Wufee_model extends CI_Model 
{

    private $_EE;
    private $_wufoo;
    private $_package_name		= 'Wufee';
    private $_package_version	= '1.5';
    private $_base_qs;
    private $_base_url;
   	private $_wufoo_api_key;
	private $_wufoo_subdomain;
	private $_wufoo_domain 		= 'wufoo.com';
    
    public  $member_id;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_EE =& get_instance();
		$this->_base_qs = 'C=addons_modules' .AMP .'M=show_module_cp' .AMP .'module=wufee';
		$this->_base_url = @BASE . AMP .$this->_base_qs;
		$this->member_id = $this->_EE->session->userdata('member_id');

		if($this->get_installed_version() != '')
		{
	        $settings = $this->get_member_settings();
	        if($settings)
	        {
		        $this->_wufoo_api_key 	= $settings->api_key;
		        $this->_wufoo_subdomain = $settings->subdomain;
			
				$this->_wufoo = new WufooApiWrapper($this->_wufoo_api_key, $this->_wufoo_subdomain, $this->_wufoo_domain);
			}
        }        
    }
    
    /**
     * Returns the installed package version.
     *
     * @access  public
     * @return  string
     */
    public function get_installed_version()
    {
        $result = $this->_EE->db->select('module_version')
        	->get_where('modules', array('module_name' => $this->get_package_name()), 1);
        	
        return $result->num_rows() === 1 ? $result->row()->module_version : '';
    }
    
    /**
     * Returns the package name.
     *
     * @access  public
     * @return  string
     */
    public function get_package_name()
    {
        return $this->_package_name;
    }
    
    /**
     * Returns the package version.
     *
     * @access  public
     * @return  string
     */
    public function get_package_version()
    {
        return $this->_package_version;
    }
    
    /**
     * Returns the base query string.
     *
     * @access  public
     * @return  string
     */
    public function get_base_qs()
    {
        return $this->_base_qs;
    }
    
    /**
     * Returns the base module URL.
     *
     * @access  public
     * @return  string
     */
    public function get_base_url()
    {
        return $this->_base_url;
    }

    /**
     * Returns the module URL with session ID if required.
     *
     * @access  public
     * @return  string
     */
    public function get_module_url()
    {
        $s = 0;
        switch(ee()->config->item('admin_session_type'))
        {
            case 's'    :
                $s = ee()->session->userdata('session_id', 0);
                break;
            case 'cs'   :
                $s = ee()->session->userdata('fingerprint', 0);
                break;
        }

        return SELF . str_replace('&amp;', '&', '?S=' . $s) . AMP . 'D=cp' . AMP . 'C=addons_modules' . AMP . 'M=show_module_cp' . AMP . 'module=wufee';
    }
    
    /**
     * Installs the module.
     *
     * @access  public
     * @return  bool
     */
    public function install_module()
    {
        $this->install_module_register();
        $this->install_module_members_table();
        $this->install_module_forms_table();
        $this->install_module_members_forms_table();
        $this->install_module_entries_table();
        $this->install_module_reports_table();
        $this->install_module_members_reports_table();

        return TRUE;
    }

    /**
     * Registers the module in the database.
     *
     * @access  public
     * @return  void
     */
    public function install_module_register()
    {
        $this->_EE->db->insert('modules', array(
            'module_name'           => ucfirst($this->get_package_name()),
            'module_version'        => $this->get_package_version(),
            'has_cp_backend'        => 'y',
            'has_publish_fields'    => 'n',
        	));
    }
    
    /**
     * Creates the Wufee members table.
     * Stores each members module settings/preferences.
     *
     * @access  public
     * @return  void
     */
    public function install_module_members_table()
    {
		$this->_EE->load->dbforge();
				
		$fields = array(
			'id' => array(
				'type' 			 => 'INT',
				'constraint'  	 => 10,
				'unsigned'		 => TRUE,
				'auto_increment' => TRUE
				),
			'member_id' => array(
				'type' 			=> 'INT',
				'constraint' 	=> 10,
				'unsigned'		=> TRUE
				),
			'subdomain' => array(
				'type'			=> 'VARCHAR',
				'constraint'	=> 100
				),
			'api_key'	=> array(
				'type'			=> 'VARCHAR',
				'constraint'	=> 100
				),
			'refresh'	=> array(
				'type'			=> 'INT',
				'constraint'	=> 2
				),
			'hash'	=> array(
				'type'			=> 'VARCHAR',
				'constraint'	=> 20
				),
			'data'	=> array(
				'type'			=> 'BLOB',
				),
			'last_sync' => array(
				'type'			=> 'DATETIME',
				'null'			=> TRUE
				)
			);
			
		$this->_EE->dbforge->add_field($fields);
		$this->_EE->dbforge->add_key('id', TRUE);
		$this->_EE->dbforge->create_table('wufee_members', TRUE);
    }
    
    /**
     * Creates the Wufee forms table.
     * Stores members Wufoo forms they have access too.
     *
     * @access  public
     * @return  void
     */
    public function install_module_forms_table()
    {
		$this->_EE->load->dbforge();
				
		$fields = array(
			'id' => array(
				'type' 			 => 'INT',
				'constraint'  	 => 10,
				'unsigned'		 => TRUE,
				'auto_increment' => TRUE
				),
			'hash' => array(
				'type' 			 => 'VARCHAR',
				'constraint' 	 => 20,
				),
			'url' => array(
				'type' 			 => 'VARCHAR',
				'constraint' 	 => 255,
				),
			'data' => array(
				'type'			 => 'BLOB',
				),
			'fields' => array(
				'type'			 => 'BLOB',
				),
			'display_fields' => array(
				'type'			 => 'BLOB',
				),
			'date_updated' => array(
				'type'			 => 'DATETIME',
				),
			'last_sync'	=> array(
				'type'			 => 'DATETIME',
				'null'			 => TRUE
				),
			);
			
		$this->_EE->dbforge->add_field($fields);
		$this->_EE->dbforge->add_key('id', TRUE);
		$this->_EE->dbforge->create_table('wufee_forms', TRUE);
    }
    
    /**
     * Creates the Wufee members forms table.
     * Relational table connecting members with forms.
     *
     * @access  public
     * @return  void
     */
    public function install_module_members_forms_table()
    {
		$this->_EE->load->dbforge();
				
		$fields = array(
			'form_id' => array(
				'type' 			=> 'INT',
				'constraint' 	=> 10,
				'unsigned'		=> TRUE
				),
			'member_id' => array(
				'type' 			=> 'INT',
				'constraint' 	=> 10,
				'unsigned'		=> TRUE
				)
			);
			
		$this->_EE->dbforge->add_field($fields);
		$this->_EE->dbforge->add_key('form_id', TRUE);
		$this->_EE->dbforge->add_key('member_id', TRUE);
		$this->_EE->dbforge->create_table('wufee_members_forms', TRUE);
    }

    /**
     * Creates the Wufee memberd forms table.
     * Relational table connecting members with forms.
     *
     * @access  public
     * @return  void
     */
    public function install_module_entries_table()
    {
		$this->_EE->load->dbforge();
				
		$fields = array(
			'id' => array(
				'type' 			 => 'INT',
				'constraint'  	 => 10,
				'unsigned'		 => TRUE,
				),
			'form_hash' => array(
				'type' 			=> 'VARCHAR',
				'constraint' 	=> 20,
				),
			'data' => array(
				'type'			=> 'BLOB'
				),
			'date_updated' => array(
				'type'			=> 'DATETIME',
				),
			);
			
		$this->_EE->dbforge->add_field($fields);
		$this->_EE->dbforge->add_key('id', TRUE);
		$this->_EE->dbforge->add_key('form_hash', TRUE);
		$this->_EE->dbforge->create_table('wufee_entries', TRUE);
    }

    /**
     * Creates the Wufee reports table.
     * Stores members Wufoo reports they have access too.
     *
     * @access  public
     * @return  void
     */
    public function install_module_reports_table()
    {
		$this->_EE->load->dbforge();
				
		$fields = array(
			'id' => array(
				'type' 			 => 'INT',
				'constraint'  	 => 10,
				'unsigned'		 => TRUE,
				'auto_increment' => TRUE
				),
			'hash' => array(
				'type' 			 => 'VARCHAR',
				'constraint' 	 => 20,
				),
			'url' => array(
				'type' 			 => 'VARCHAR',
				'constraint' 	 => 255,
				),
			'data' => array(
				'type'			 => 'BLOB',
				),
			'date_updated' => array(
				'type'			 => 'DATETIME',
				),
			);
			
		$this->_EE->dbforge->add_field($fields);
		$this->_EE->dbforge->add_key('id', TRUE);
		$this->_EE->dbforge->create_table('wufee_reports', TRUE);
    }

    /**
     * Creates the Wufee members reports table.
     * Relational table connecting members with reports.
     *
     * @access  public
     * @return  void
     */
    public function install_module_members_reports_table()
    {
		$this->_EE->load->dbforge();
				
		$fields = array(
			'report_id' => array(
				'type' 			=> 'INT',
				'constraint' 	=> 10,
				'unsigned'		=> TRUE
				),
			'member_id' => array(
				'type' 			=> 'INT',
				'constraint' 	=> 10,
				'unsigned'		=> TRUE
				)
			);
			
		$this->_EE->dbforge->add_field($fields);
		$this->_EE->dbforge->add_key('report_id', TRUE);
		$this->_EE->dbforge->add_key('member_id', TRUE);
		$this->_EE->dbforge->create_table('wufee_members_reports', TRUE);
    }

    /**
     * Uninstalls the module.
     *
     * @access  public
     * @return  bool
     */
    public function uninstall_module()
    {
        $module_name = ucfirst($this->get_package_name());

        // Retrieve the module information.
        $result = $this->_EE->db
            ->select('module_id')
            ->get_where('modules', array('module_name' => $module_name), 1);

        if($result->num_rows() !== 1)
        {
            return FALSE;
        }

        $this->_EE->db->delete('module_member_groups', array('module_id' => $result->row()->module_id));
        $this->_EE->db->delete('modules', array('module_name' => $module_name));
        
        // Drop the module entries table.
        $this->_EE->load->dbforge();
        $this->_EE->dbforge->drop_table('wufee_members');
        $this->_EE->dbforge->drop_table('wufee_forms');
        $this->_EE->dbforge->drop_table('wufee_members_forms');
        $this->_EE->dbforge->drop_table('wufee_entries');
        $this->_EE->dbforge->drop_table('wufee_reports');
        $this->_EE->dbforge->drop_table('wufee_members_reports');

        return TRUE;
    }
    
    /**
     * Updates the module.
     *
     * @access  public
     * @param   string      $installed_version      The installed version.
     * @param   bool        $force                  Forcibly update the module version number?
     * @return  bool
     */
    public function update_package($installed_version = '', $force = FALSE)
    {
        if(version_compare($installed_version, $this->get_package_version(), '>='))
        {
            return FALSE;
        }

        /*if(version_compare($installed_version, '1.4', '<'))
        {
            $this->_update_package_to_version_14();
        }*/

        // Forcibly update the module version number?
        if($force === TRUE)
        {
            $this->_ee->db->update(
                'modules',
                array('module_version' => $this->get_package_version()),
                array('module_name' => $this->get_package_name())
            );
        }

        return TRUE;
    }
            
    /**
     * Returns the package theme folder URL, appending a forward slash if required.
     *
     * @access    public
     * @return    string
     */
    public function get_package_theme_url()
    {
        $theme_url = $this->_EE->config->item('theme_folder_url');
        $theme_url .= substr($theme_url, -1) == '/' ? 'third_party/' : '/third_party/';

        return $theme_url . strtolower($this->get_package_name()) . '/';
    }
    
    /**
     * Returns the front end forms URL for selected account.
     *
     * @access    public
     * @return    string
     */
    public function get_wufoo_account_url()
    {
    	return 'http://' . $this->_wufoo_subdomain . '.' . $this->_wufoo_domain . '/';
    }
    
    /**
     * Returns the Wufoo account subdomain from settings.
     *
     * @access    public
     * @return    string
     */
    public function get_wufoo_subdomain()
    {
    	return $this->_wufoo_subdomain;
    }
    
    /**
     * Returns module settings for current user.
     *
     * @access    public
     * @param	  member_id
     * @return    string
     */
    public function get_member_settings()
    {
		$qry = $this->_EE->db->select('*')
			->from('wufee_members')
			->where('id', 1)
			->get();
			
		if($qry->num_rows() > 0)
		{
			return $qry->row();
		}
		else
		{
			return FALSE;
		}
    }
    
    /**
     * Update members module settings in DB.
     *
     * @access    public
     * @param	  setting params
     * @return    string
     */
    public function update_member_settings($params)
    {
    	if(!is_object($this->_wufoo) OR (isset($params['api_key']) AND $params['api_key'] != $this->_wufoo_api_key))
    	{
			$this->_wufoo = new WufooApiWrapper($params['api_key'], $params['subdomain'], $this->_wufoo_domain);
    	}
    
    	foreach($this->_wufoo->getUsers() as $hash => $user)
    	{
    		if(array_key_exists('api_key', $params) AND $params['api_key'] == $user->ApiKey)
    		{
    			$params['hash'] = $hash;
    			$params['data'] = serialize($user);
    			break;
    		}
    	}
    
    	if($this->_EE->db->select('*')->from('wufee_members')->where('member_id', $this->member_id)->count_all_results() > 0)
    	{
    		$this->_EE->db->update('wufee_members', $params, array('member_id' => $this->member_id));
    	}
    	else
    	{
    		$params['member_id'] = $this->member_id;
    		$this->_EE->db->insert('wufee_members', $params);
    	}
    }
    
    /**
     * Sync form data from API with DB.
     *
     * @access    public
     * @return    string
     */
    public function sync_forms($force = FALSE)
    {
    	// check to see if member currently has any forms in table
    	// if not, sync automatically because they may be new
    	$settings = $this->get_member_settings();
    	if(is_null($settings->last_sync))
    	{
	    	foreach($this->_wufoo->getForms() as $hash => $form)
	    	{
	    		$form->TotalEntries = $this->_wufoo->getEntryCount($hash);
	    		$form->TotalEntriesToday = $form->EntryCountToday;
	    		
	    		$fields = $this->_wufoo->getFields($hash);
	    		$i = 1;
	    		$display_fields = '';
	    		foreach($fields->Fields as $field)
	    		{
					$display_fields .= $field->ID;

					if($i < 4)
					{
						$display_fields .= '|';
						++$i;
					}	    
					else
					{
						break;
					}			
	    		}
	    		
	    		$form_params = array(
	    			'hash' 				=> $hash,
	    			'url'				=> $form->Url,
	    			'data' 				=> serialize($form),
	    			'fields' 			=> serialize($fields),
	    			'display_fields' 	=> $display_fields,
	    			'date_updated'		=> date('Y-m-d H:i:s')
	    			);
	    		$display_fields = '';
	    		$this->_EE->db->insert('wufee_forms', $form_params);
	    		$this->_EE->db->insert('wufee_members_forms', array('member_id' => $this->member_id, 'form_id' => $this->_EE->db->insert_id()));

				$this->sync_reports();
				
				$this->update_member_settings(array('last_sync' => date('Y-m-d H:i:s')));	
	    	}
    	}
    	else
    	{
			if($force OR date('Y-m-d H:i:s', (strtotime($settings->last_sync)+$settings->refresh*60)) < date('Y-m-d H:i:s'))
			{
				$current_forms = $this->get_form_options();
				$forms = $this->_wufoo->getForms();
			
				// update existing forms
	    		foreach($forms as $hash => $form)
	    		{
	    			if(array_key_exists($hash, $current_forms))
	    			{
			    		$form->TotalEntries = $this->_wufoo->getEntryCount($hash);
			    		$form->TotalEntriesToday = $form->EntryCountToday;
		    		
			    		$form_params = array(
			    			'url'				=> $form->Url,
			    			'data' 				=> serialize($form),
			    			'fields' 			=> serialize($this->_wufoo->getFields($hash)),
			    			'date_updated'		=> date('Y-m-d H:i:s')
			    			);
			    		$this->_EE->db->update('wufee_forms', $form_params, array('hash' => $hash));
			    		
			    		unset($forms[$hash]);	    		
			    	}
	    		}
	    		
	    		// add new forms
		    	foreach($forms as $hash => $form)
		    	{
		    		$form->TotalEntries = $this->_wufoo->getEntryCount($hash);
		    		$form->TotalEntriesToday = $form->EntryCountToday;
		    		
		    		$fields = $this->_wufoo->getFields($hash);
		    		$i = 1;
		    		$display_fields = '';
		    		foreach($fields->Fields as $field)
		    		{
						$display_fields .= $field->ID;
	
						if($i < 4)
						{
							$display_fields .= '|';
							++$i;
						}	    
						else
						{
							break;
						}			
		    		}
		    		
		    		$form_params = array(
		    			'hash' 				=> $hash,
		    			'url'				=> $form->Url,
		    			'data' 				=> serialize($form),
		    			'fields' 			=> serialize($fields),
		    			'display_fields' 	=> $display_fields,
		    			'date_updated'		=> date('Y-m-d H:i:s')
		    			);
		    		$display_fields = '';
		    		$this->_EE->db->insert('wufee_forms', $form_params);
		    		$this->_EE->db->insert('wufee_members_forms', array('member_id' => $this->member_id, 'form_id' => $this->_EE->db->insert_id()));
		    	}

				$this->sync_reports();
				
				$this->update_member_settings(array('last_sync' => date('Y-m-d H:i:s')));	
			}
		}			
	}
	
    /**
     * Sync entry data from API with DB.
     *
     * @access    public
     * @return    string
     */
	public function sync_entries($hash, $force = FALSE)
	{
		$xform = $this->get_form($hash);
		$form = unserialize($xform->data);
		
		$page_size = 100; // max allowed by API: 100
		
		if(is_null($xform->last_sync))
		{
			if($form->TotalEntries > $page_size)
			{
				$page = 0;
				while(($form->TotalEntries/$page_size) > $page)
				{
					foreach($this->_wufoo->getEntries($hash, 'forms', 'pageStart=' . $page . '&pageSize=' . $page_size) as $entry_id => $entry)
					{
						$entry_params = array(
							'id' 			=> $entry_id,
							'form_hash'		=> $hash,
							'data' 			=> serialize($entry),
							'date_updated' 	=> date('Y-m-d H:i:s')
							);
						$this->_EE->db->insert('wufee_entries', $entry_params);
					}	
					++$page;
				}
			}
			else
			{
				foreach($this->_wufoo->getEntries($hash, 'forms', 'pageSize=' . $page_size) as $entry_id => $entry)
				{
					$entry_params = array(
						'id' 			=> $entry_id,
						'form_hash'		=> $hash,
						'data' 			=> serialize($entry),
						'date_updated' 	=> date('Y-m-d H:i:s')
						);
					$this->_EE->db->insert('wufee_entries', $entry_params);
				}	
			}
		}
		else
		{
			$settings = $this->get_member_settings();
			if((date('Y-m-d H:i:s', (strtotime($xform->last_sync)+$settings->refresh*60)) < date('Y-m-d H:i:s')) OR $force)
			{	
				$entry_id = $this->_EE->db->select_max('id')->get('wufee_entries')->row()->id;
				$new_entries = $this->_wufoo->getEntries($hash, 'forms', 'pageSize=' . $page_size . '&Filter1=' . urlencode('EntryId Is_greater_than ' . $entry_id) . '&match=AND');
	       		foreach($new_entries as $entry_id => $entry)
	    		{
	    			$entry_params = array(
	    				'id' 			=> $entry_id,
	    				'form_hash'		=> $hash,
	    				'data' 			=> serialize($entry),
	    				'date_updated' 	=> date('Y-m-d H:i:s')
	    				);
	    			$this->_EE->db->insert('wufee_entries', $entry_params);
	    		}
	    		
	       		foreach($this->_wufoo->getEntries($hash, 'forms', 'pageSize=' . $page_size . '&Filter1=' . urlencode('DateUpdated Is_after ' . $xform->last_sync) . '&match=AND') as $entry_id => $entry)
	    		{
	    			$entry_params = array(
	    				'data' 			=> serialize($entry),
	    				'date_updated' 	=> date('Y-m-d H:i:s')
	    				);
	    			$this->_EE->db->update('wufee_entries', $entry_params, array('id' => $entry_id, 'form_hash' => $hash));
	    		}
			}
    	}
    	
    	$this->update_form($hash, array('last_sync' => date('Y-m-d H:i:s')));	
	}
	
    /**
     * Sync report data from API with DB.
     *
     * @access    public
     * @return    string
     */
	public function sync_reports()
	{
    	$settings = $this->get_member_settings();
    	if(is_null($settings->last_sync))
    	{
			foreach($this->_wufoo->getReports() as $hash => $report)
			{
	    		$report_params = array(
	    			'hash' 			=> $hash,
	    			'url'			=> $report->Url,
	    			'data' 			=> serialize($report),
	    			'date_updated'	=> date('Y-m-d H:i:s')
	    			);
	    		$this->_EE->db->insert('wufee_reports', $report_params);
	    		$this->_EE->db->insert('wufee_members_reports', array('member_id' => $this->member_id, 'report_id' => $this->_EE->db->insert_id()));
			}
		}
		else
		{
		
		}
	}
    	    
    /**
     * Returns array of forms current user has access too.
     *
     * @access    public
     * @param	  member_id
     * @return    array
     */
    public function get_forms()
    {
    	return $this->_EE->db->select('wufee_forms.*')
    		->from('wufee_forms')
    		->join('wufee_members_forms', 'wufee_forms.id = wufee_members_forms.form_id')
    		->get()
    		->result();
    }
    
    /**
     * Returns Wufoo form object for selected form.
     *
     * @access    public
     * @param	  Wufoo form hash
     * @return    object
     */
    public function get_form($hash)
    {
    	return $this->_EE->db->select('wufee_forms.*')
    		->from('wufee_forms')
    		->join('wufee_members_forms', 'wufee_forms.id = wufee_members_forms.form_id')
    		->where('wufee_forms.hash', $hash)
    		->get()
    		->row();
    }
    
    /**
     * Returns Wufoo form object for selected form.
     *
     * @access    public
     * @param	  Wufoo form url
     * @return    object
     */
    public function get_form_by_url($url)
    {
    	return $this->_EE->db->select('wufee_forms.*')
    		->from('wufee_forms')
    		->join('wufee_members_forms', 'wufee_forms.id = wufee_members_forms.form_id')
    		->where('wufee_forms.url', $url)
    		->get()
    		->row();
    }
    
    /**
     * Returns array formatted ID => Label for form drop down menu. Used by field type.
     *
     * @access    public
     * @return    array
     */
    public function get_form_options()
    {
    	$forms = $this->get_forms();
    	
    	$options = array();
    	foreach($forms as $form)
    	{
    		$data = unserialize($form->data);
    		$options[$form->hash] = $data->Name;
    	}
    	
    	return $options;
    }
    
    /**
     * Attempt to update a form in the DB.
     *
     * @access    public
     * @param     string
     * @param     array
     * @return    void
     */
    public function update_form($hash, $params)
    {
    	return $this->_EE->db->update('wufee_forms', $params, array('hash' => $hash));
    }
        
    /**
     * Returns array of entries for selected form.
     *
     * @access    public
     * @param	  Wufoo form hash
     * @return    object
     */
    public function get_entries($hash, $limit = 25, $offset = 0)
    {
    	return $this->_EE->db->select('wufee_entries.*')
    		->from('wufee_entries')
    		->join('wufee_forms', 'wufee_entries.form_hash = wufee_forms.hash')
    		->join('wufee_members_forms', 'wufee_forms.id = wufee_members_forms.form_id')
    		->where('wufee_members_forms.member_id', $this->member_id)
    		->where('wufee_entries.form_hash', $hash)
    		->limit($limit, $offset)
    		->order_by('wufee_entries.id DESC')
    		->get()
    		->result();
    }
    
    /**
     * Returns single requested entry.
     *
     * @access    public
     * @param	  Wufoo form hash
     * @param	  Wufoo entry ID
     * @return    object
     */
    public function get_entry($hash, $entry_id)
    {
    	$row = $this->_EE->db->select('wufee_entries.*')
    		->from('wufee_entries')
    		->join('wufee_forms', 'wufee_entries.form_hash = wufee_forms.hash')
    		->join('wufee_members_forms', 'wufee_forms.id = wufee_members_forms.form_id')
    		->where('wufee_members_forms.member_id', $this->member_id)
    		->where('wufee_entries.form_hash', $hash)
    		->where('wufee_entries.id', $entry_id)
    		->get()
    		->row();
    		
    	return unserialize($row->data);
    }
    
    /**
     * Returns array of reports user has access too.
     *
     * @access    public
     * @return    array
     */
    public function get_reports()
    {
    	return $this->_EE->db->select('wufee_reports.*')
    		->from('wufee_reports')
    		->join('wufee_members_reports', 'wufee_reports.id = wufee_members_reports.report_id')
    		->where('wufee_members_reports.member_id', $this->member_id)
    		->get()
    		->result();
    }
}
/* End of file wufee_model.php */
/* Location: /system/expressionengine/third_party/wufee/models/wufee_model.php */