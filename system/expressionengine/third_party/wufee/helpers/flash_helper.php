<?php

/**
 * Flash Helper
 *
 * Provide helper functions for common flash message operations.
 *
 * @author		Chris Monnat
 */
 
/** 
 * Display formatted flash message.
 * 
 * @access public 
 * @param string
 * @return string
 */ 
function display_flash($name)
{
	$EE =& get_instance();
	
	if($EE->session->flashdata($name)) 
	{
		$flash = $EE->session->flashdata($name);
		return '<div class="alert-message '.$flash['type'].'">
					<a class="close" href="#">×</a>
					<p>'.$flash['msg'].'</p>
				</div>';
	}
}

/** 
 * Save provided message as a flash variable.
 * 
 * @access public 
 * @param string
 * @param string
 * @param string
 * @return void
 */ 
function set_flash($name, $type, $msg)
{
	$EE =& get_instance();
	$EE->session->set_flashdata($name, array('type' => $type, 'msg' => $msg));
}

/* End of file flash_helper.php */ 
/* Location: ./application/helpers/flash_helper.php */ 