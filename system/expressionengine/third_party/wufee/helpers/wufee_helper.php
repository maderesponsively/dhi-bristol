<?php

/** 
 * Change a MySQL formatted date into a normal date.
 * 
 * @access public 
 * @param string
 * @return string
 */ 
function mysql_to_human($date) 
{
    if($date != '' && $date != '0000-00-00') 
	{
        $parts = explode('-', $date);
    	return $parts[1].'/'.$parts[2].'/'.$parts[0];
    }
}

/** 
 * Change a MySQL datetime formatted string to a normal formatted string.
 * 
 * @access public 
 * @param string
 * @return string
 */ 
function datetime_to_human($date)
{
    if($date != '') {
        $date_parts = explode(' ', $date);
        $time_parts = explode(':', $date_parts[1]);
        if($time_parts[0] > 12)
        {
        	$time = $time_parts[0] - 12;
        	$suffix = 'PM';
        }
        else
        {
        	$time = $time_parts[0];
        	$suffix = 'AM';
        }
        $time .= ':' . $time_parts[1] . ' ' . $suffix;
    	return mysql_to_human($date_parts[0]).' '.$time;
    }
}
