<?php

/**
 * Wufee Reports Widget
 *
 * Display listing of Wufee reports.
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Widget
 * @author		Chris Monnat
 * @link		http://chrismonnat.com
 */

class Wgt_reports
{
	public $title;		// title displayed at top of widget
	public $wclass;		// class name for additional styling capabilities
	
	private $_EE;
	private $_model;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{	
		$this->title  	= 'Wufoo Reports';
		$this->wclass 	= 'padded';	
		
		$this->_EE 		=& get_instance();
	}
	
	// ----------------------------------------------------------------
	
	/**
	 * Permissions Function
	 * Defines permissions needed for user to be able to add widget.
	 *
	 * @return 	bool
	 */
	public function permissions()
	{
		$user_modules = $this->_EE->session->userdata('assigned_modules');
		
		if($this->_EE->session->userdata('group_id') != 1 AND !in_array('wufee', $user_modules))
		{
			return FALSE;
		}
		
		return TRUE;
	}

	/**
	 * Display list of forms and # of entries.
	 *
	 * @return 	string
	 */
	public function index()
	{
		$this->_EE->load->model('wufee_model');
		$model = $this->_EE->wufee_model;
	
		$page_data = array(
			'wufoo_url' => $model->get_wufoo_account_url(),
			'reports' 	=> $model->get_reports(),
			);
				
		return $this->_EE->load->view('wgt_reports', $page_data, TRUE);
	}	
}
/* End of file wgt.wufee_reports.php */
/* Location: /system/expressionengine/third_party/wufee/widgets/wgt.reports.php */