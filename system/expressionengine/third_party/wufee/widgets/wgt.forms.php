<?php

/**
 * Wufee Forms Widget
 *
 * Display listing of Wufee forms with links to module.
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Widget
 * @author		Chris Monnat
 * @link		http://chrismonnat.com
 */

class Wgt_forms
{
	public $title;		// title displayed at top of widget
	public $wclass;		// class name for additional styling capabilities
	
	private $_EE;
	private $_model;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{	
		$this->title  	= 'Wufoo Forms';
		$this->wclass 	= 'contentMenu';	
		
		$this->_EE 		=& get_instance();
	}
	
	// ----------------------------------------------------------------
	
	/**
	 * Permissions Function
	 * Defines permissions needed for user to be able to add widget.
	 *
	 * @return 	bool
	 */
	public function permissions()
	{
		$user_modules = $this->_EE->session->userdata('assigned_modules');
		
		if($this->_EE->session->userdata('group_id') != 1 AND !in_array('wufee', $user_modules))
		{
			return FALSE;
		}
		
		return TRUE;
	}

	/**
	 * Display list of forms and # of entries.
	 *
	 * @return 	string
	 */
	public function index()
	{
		$this->_EE->load->model('wufee_model');
		$model = $this->_EE->wufee_model;
	
		$page_data = array(
			'base_url' 	=> $model->get_base_url(),
			'forms' 	=> $model->get_forms(),
			);
			
		return $this->_EE->load->view('wgt_forms', $page_data, TRUE);
	}	
}
/* End of file wgt.wufee_forms.php */
/* Location: /system/expressionengine/third_party/wufee/widgets/wgt.forms.php */