//script: DC_toolbar
//description: creates accessibility toolbar
//author: Marcus Kielly
//company: Deckchair Designs
//date: 8/12/09
//dependecies: jQuery 1.3


function DC_toolbar(){

    if($.cookie('bar')=='none'){
      return;
    }

    var loc = window.location.href;//get url for current location

    var main = $('header');
    var content = $('.resize');

    switch($.cookie("size")){
      case "small":
      content.css('font-size','100%');
      break;

      case "medium":
      content.css('font-size','130%');
      break;

      case "large":
      content.css('font-size','150%');
      break;

      default:
      content.css('font-size','100%');
    }

    //create the top link
    $("<a name='top' id='goto_top_link'>top</a>").insertBefore(main);

    //remove any previous instances of the anchor call for the top link button
     var topPtn = /#top/;
     if(topPtn.test(loc)){
       loc = loc.replace('#top','');
     };



    //prepare the bar contents
     var accessContents = '<div id="text_size">';
     accessContents += "<p class='button'>Text Size</p>";
     accessContents += "<ul>";
     accessContents += "<li id='small' class='button'>Small</li>";
     accessContents += "<li id='medium' class='button'>Medium</li>";
     accessContents += "<li id='large' class='button'>Large</li>";
     accessContents += "</ul>";
     accessContents += "</div>";
     accessContents += "<div id='close'>";
     accessContents += "<p id='close_tip'>closes permanently, reset cookies to restore</p>";
     accessContents += "<p class='button'>Close</p>";
     accessContents += "<p id='close_button' class='button'>Close</p>";
     accessContents += "</div>";
     accessContents += "<div id='top_link'>";
     accessContents += "<a href='" + loc + "#top' class='button'>Back to the top</a>";
     accessContents += "</div>";
	 accessContents += "<div id='cookie_link'>";
     accessContents += "<a href='/cookies-policy' class='button'>Cookies</a>";
     accessContents += "</div>";

   //insert the access bar
    var accessBarDiv = document.createElement('div');
    accessBarDiv.setAttribute('id','access_bar');
    var body = $('body');
    body.append(accessBarDiv);

    var accessBar = $('#access_bar');
    accessBar.insertAfter('#wrapper');
    accessBar.html(accessContents);

    // locate bar elements
    var closeButton = $('p#close_button');
    var closeTip = $('p#close_tip');
    var smallButton = $('li#small');
    var mediumButton = $('li#medium');
    var largeButton = $('li#large');


    smallButton.click(function(){
        content.css('font-size','100%');
        $('div#intro_text').css('overflow','hidden');
        $.cookie("size","small");
    })
    mediumButton.click(function(){
        content.css('font-size','130%');
        $.cookie("size","medium");
    })
    largeButton.click(function(){
        content.css('font-size','150%');
        $.cookie("size","large");
    })

    closeButton.mouseover(function(){
      closeTip.fadeIn('fast');
    })

    closeButton.mouseout(function(){
      closeTip.fadeOut('fast');
    })

    closeButton.click(function(){
        //version 1: set cookie to remove
        accessBar.fadeOut({
            callback: function() {
            this.remove();
            }
        });
        //set a cookie to record the value
        $.cookie("bar", "none", { expires: 1 });
        //return false;
    })

    //exit the routine before the hide show buttons are inserted
    
		//if($.browser.msie && $.browser.version=="6.0"){
   	//   return;
   	//}

    //add the display button
    var displayButtonDiv = document.createElement('div');
    displayButtonDiv.setAttribute('id','display');
    accessBar.append(displayButtonDiv);

    var displayButton = $('#display');

    //define contents of display div
    var displayButtonContents = '<div id="show"><p class="button">Show</p></div>';
    displayButtonContents += '<div id="hide"><p class="button">Hide</p></div>';
    displayButton.html(displayButtonContents);

    var accessBarShow = $('#show');
    var accessBarHide = $('#hide');

    if($.cookie('DC_access')=='hide'){
        accessBar.css('bottom','-30px');
        accessBarHide.hide();
        accessBarShow.show();
    }
    if($.cookie('DC_access')=='show'){
        accessBar.css('bottom','0px');
        accessBarShow.hide();
        accessBarHide.show();
    }

    accessBarShow.click(function() {
          accessBar.animate({bottom:'0px'},200);
          accessBarHide.fadeIn('fast');
          $(this).fadeOut('fast');
          $.cookie('DC_access', 'show', { expires: 1 });
          return false;
        });

    accessBarHide.click(function() {
          accessBar.animate({bottom:'-30px'},200);
          accessBarShow.fadeIn('fast');
          $(this).fadeOut('fast');
          $.cookie("DC_access", "hide", { expires: 1 });
          return false;
        });
}