$().ready(function() {

	$.ajax({
		type: 'GET',
		url: EE.BASE + '&C=addons_modules&M=show_module_cp&module=wufee&method=show_entries&form_id='+$.getUrlVar('form_id')+'&refresh='+$.getUrlVar('refresh')+'&ajax=yes',
		dataTyle: 'html',
		success: function(html) {
			$('#mainContent .pageContents').html(html);
		},
		error: function(html) {
			$('#mainContent .pageContents').html('<p>There was a problem.</p>');
		}
	});

});